module ITCPG

#*------------------------------------------------------------------------------
#* Load packages

using CSV
using DataFrames
using JLD2
using FileIO
using PowerModels
using Ipopt
using DataStructures
using NetCDF
using Statistics
using Memento
using Suppressor
using Random
using Distributions

#*------------------------------------------------------------------------------
#* Setup logger for generating .log files during simulations

function __init__()
    ### Setup logger (PowerModels.jl logger will inherit the format)
    global LOGGER = Memento.config!(
        "info", fmt="[{date} | {level} | {name}]: {msg}"
    )
end

#=
Changes the logger level to the passed level. Common options are "info" (default) and "debug" (additional logs for debugging)
=#
function config_logger(level="info")
    info(LOGGER, "Logger level set to $(level)!")
    setlevel!(LOGGER, level)
    return nothing
end

export config_logger

#*------------------------------------------------------------------------------
#* Include other code files and export functions

include("Data.jl")
export build_network_data, aggregate_generators!
export add_locs!, add_tl_lengths!, add_tl_voltages!
export get_bustypes, get_underground_tl, get_MW_loads, get_total_load 
export get_inactive_elements
export get_branches, deactivate_overloads!, destroy_tl!
export check_voltage_bounds, check_gen_bounds

include("Analysis.jl")
export check_ac_constraints, check_nodal_power_balance 
export check_area_nodal_power_balance, check_area_power_balance

include("PowerFlow.jl")
export calc_ac_pf!, calc_dc_pf!, calc_secondary_dmg!
export update_pf_data!, calc_pf, calc_branchloads!
export calc_new_state!, run_outer_ac_pf!, run_inner_ac_pf!
export run_pvpq_switching!, run_uvls!
export restore_p_balance!, calc_Δp
export redistribute_slack_dispatches!

include("Impact.jl")
export sim_primary_dmg, eval_p_fail!
export calc_overhead_tl_windloads, calc_overhead_tl_segments 
export get_windfield, get_winddata, get_tl_failure_probs

end
