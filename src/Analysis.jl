#* Functions for analysing the state of a power grid given its data dictionary
#*------------------------------------------------------------------------------







#*------------------------------------------------------------------------------
#* Functions that check AC-PF constraints

#=
Checks and logs the constraints considered in the JuMP model that PowerModels.jl builts for the AC power flow problem. The constraints are:
1. Kirchoff's current law: AC power balance at each bus
2. Slack bus constraints: Voltage angle and magnitude fixed
3. PV-bus constraints: Voltage magnitudes fixed at setpoints
4. Generator constraints (generators are connected to PV-buses): Active power dispatches fixed
=#
function check_ac_constraints(
        new_ndd::Dict{String,<:Any}, # network data with new solution
        old_ndd::Dict{String,<:Any} # network data with old solution
    )
    
    ### Check nodal power balances (Kirchhoff's current law)
    check_nodal_ac_power_balance(new_ndd)

    ### Check constraints 2. - 4. for each connected component, respectively
    connected_components = calc_connected_components(new_ndd)
    for (cc_i, cc) in enumerate(connected_components)
        check_ac_constraints(new_ndd, old_ndd, cc, cc_i)
    end
    
    return nothing
end

#=
Calculates and logs the maximum nodal active and reactive power mismatch. Both the magnitude of the mismatch and the corresponding bus are logged.
=#
function check_nodal_ac_power_balance(network_data::Dict{String,<:Any})
    ### Calculate nodal AC power mismatches
    nodal_balance = calc_power_balance(network_data)

    ### Determine maximum active and reactive power mismatch
    max_p_delta = 0.
    max_p_delta_bus = ""
    max_q_delta = 0.
    max_q_delta_bus = ""
    for (key, bus) in nodal_balance["bus"]
        if abs(bus["p_delta"]) > max_p_delta
            max_p_delta = abs(bus["p_delta"])
            max_p_delta_bus = key
        end
        if abs(bus["q_delta"]) > max_q_delta
            max_q_delta = abs(bus["q_delta"])
            max_q_delta_bus = key
        end
    end

    ### Log results
    info(LOGGER, "Maximum nodal active power mismatch: $max_p_delta at bus $max_p_delta_bus")
    info(LOGGER, "Maximum nodal reactive power mismatch: $max_q_delta at bus $max_q_delta_bus")

    return nothing
end

function check_area_nodal_power_balance(
        network_data::Dict{String,<:Any}, 
        area = 7
    )
    
    active_area_buses = String[
        "$(b["index"])" for b in values(network_data["bus"]) if 
        b["area"] == area && b["bus_type"] != 4
    ]
    nodal_balance = calc_power_balance(network_data)["bus"]

    area_nodal_balance = Dict(
        i => values for (i, values) in nodal_balance if i ∈ active_area_buses
    )

    max_p_delta = 0.
    max_p_delta_bus = ""
    max_q_delta = 0.
    max_q_delta_bus = ""

    for (key, bus) in area_nodal_balance
        if abs(bus["p_delta"]) > max_p_delta
            max_p_delta = abs(bus["p_delta"])
            max_p_delta_bus = key
        end
        if abs(bus["q_delta"]) > max_q_delta
            max_q_delta = abs(bus["q_delta"])
            max_q_delta_bus = key
        end
    end

    info(LOGGER, "Area $area: Maximum nodal active power mismatch: $max_p_delta at bus $max_p_delta_bus")
    info(LOGGER, "Area $area: Maximum nodal reactive power mismatch: $max_q_delta at bus $max_q_delta_bus")

    return nothing
end

function check_ac_constraints(
        new_ndd::Dict{String,<:Any}, # network data with new solution
        old_ndd::Dict{String,<:Any}, # network data with old solution
        cc::Set{Int64}, # set of bus indices in connected component
        cc_i::Int64 # index/number of connected component
    )

    ### Check slack constraints
    cc_slack_new = [
        b for b in values(new_ndd["bus"]) if b["bus_type"]==3 && b["index"]∈cc
    ] # slack bus in new network data
    cc_slack_old = [
        b for b in values(old_ndd["bus"]) if b["bus_type"]==3 && b["index"]∈cc
    ] # slack bus in old network data
    if length(cc_slack_new) != 1
        warn(LOGGER, "Grid #$cc_i has $(length(cc_slack_new)) slack buses!")
    elseif cc_slack_new[1]["index"] != cc_slack_old[1]["index"]
        warn(LOGGER, 
            "Slack bus $(cc_slack_new[1]["index"]) in the new state of $cc_i does not agree with slack bus $(cc_slack_old[1]["index"]) in the old state!"
        )
    end
    cc_slack_gen = [
        gen["index"] for gen in values(new_ndd["gen"]) if 
        gen["gen_bus"] == cc_slack_new[1]["index"]
    ] # index of generator connected to the slack bus
    info(LOGGER, 
        "Grid #$cc_i: Slack bus $(cc_slack_new[1]["index"]) (with generator $cc_slack_gen) voltage angle constraint: va_new = $(cc_slack_new[1]["va"]), va_old = $(cc_slack_old[1]["va"])"
    )
    info(LOGGER, 
        "Grid #$cc_i: Slack bus $(cc_slack_new[1]["index"]) (with generator $cc_slack_gen) voltage magnitude constraint: vm_new = $(cc_slack_new[1]["vm"]), vm_old = $(cc_slack_old[1]["vm"])"
    )
    info(LOGGER, 
        "Grid #$cc_i: Slack bus $(cc_slack_new[1]["index"]) (with generator $cc_slack_gen) active power dispatch: pg_new = $(new_ndd["gen"]["$(cc_slack_gen[1])"]["pg"]), pg_old = $(old_ndd["gen"]["$(cc_slack_gen[1])"]["pg"])"
    )

    ### Check PV-bus constraints
    cc_PV_new = [
        b for b in values(new_ndd["bus"]) if b["bus_type"] == 2 && b["index"]∈cc
    ]
    max_vm_diff = 0.0 # maximum voltage magnitude mismatch
    max_vm_diff_bus = 0 # index of corresponding PV-bus
    min_vm_diff = 1e10 # minimum voltage magnitude mismatch
    min_vm_diff_bus = 0 # index of corresponding PV-bus
    # vm_mismatches = zeros(length(cc_PV_new))
    for PV_bus in cc_PV_new
        vm_diff = PV_bus["vm"] - old_ndd["bus"]["$(PV_bus["index"])"]["vm"]
        if abs(vm_diff) >= abs(max_vm_diff)
            max_vm_diff = vm_diff
            max_vm_diff_bus = PV_bus["index"]
        end
        if abs(vm_diff) < abs(min_vm_diff)
            min_vm_diff = vm_diff
            min_vm_diff_bus = PV_bus["index"]
        end
    end
    info(LOGGER, 
        "Grid #$cc_i: Maximum voltage magnitude mismatch: vm_new - vm_old = $max_vm_diff at PV-bus $max_vm_diff_bus"
    )
    info(LOGGER, 
        "Grid #$cc_i: Minimum voltage magnitude mismatch: vm_new - vm_old = $min_vm_diff at PV-bus $min_vm_diff_bus"
    )

    ### Check generator active power constraints
    active_cc_gens = [
        gen for gen in values(new_ndd["gen"]) if gen["gen_bus"] ∈ cc &&
        gen["gen_status"] != 0 && gen["index"] ∉ cc_slack_gen
    ] # active generators that are connected to buses in cc
    max_pg_diff = 0.0 # maximum mismatch in active power dispatch
    max_pg_diff_gen = 0 # index of corresponding generator
    min_pg_diff = 1e10 # minimum mismatch in active power dispatch
    min_pg_diff_gen = 0 # index of corresponding generator
    for gen in active_cc_gens
        pg_diff = gen["pg"] - old_ndd["gen"]["$(gen["index"])"]["pg"]
        if abs(pg_diff) >= abs(max_pg_diff)
            max_pg_diff = pg_diff
            max_pg_diff_gen = gen["index"]
        end
        if abs(pg_diff) < abs(min_pg_diff)
            min_pg_diff = pg_diff
            min_pg_diff_gen = gen["index"]
        end
    end
    info(LOGGER, 
        "Grid #$cc_i: Maximum mismatch in active power dispatch: pg_new - pg_old = $max_pg_diff at generator $max_pg_diff_gen"
    )
    info(LOGGER, 
        "Grid #$cc_i: Minimum mismatch in active power dispatch: pg_new - pg_old = $min_pg_diff at generator $min_pg_diff_gen"
    )

    ### Check maximum voltage angle difference along branches
    cc_branches = [
        (br["f_bus"], br["t_bus"]) for br in values(new_ndd["branch"]) if 
        br["br_status"] == 1 && br["f_bus"] ∈ cc && br["t_bus"] ∈ cc
    ]
    max_va_diff = 0.0 # maximum voltage angle difference
    max_va_diff_br = (0, 0) # from- and to-bus of corresponding branch
    min_va_diff = 1e10 # minimum voltage angle difference
    min_va_diff_br = (0, 0) # from- and to-bus of corresponding branch
    for br in cc_branches
        va_diff = new_ndd["bus"]["$(br[2])"]["va"] - new_ndd["bus"]["$(br[1])"]["va"]
        if abs(va_diff) >= abs(max_va_diff)
            max_va_diff = va_diff
            max_va_diff_br = br
        end
        if abs(va_diff) < abs(min_va_diff)
            min_va_diff = va_diff
            min_va_diff_br = br
        end
    end
    info(LOGGER, 
        "Grid #$cc_i: Maximum branch voltage angle difference: va_to - va_fr = $(rad2deg(max_va_diff))° in branch $max_va_diff_br"
    )
    info(LOGGER, 
        "Grid #$cc_i: Minimum branch voltage angle difference: va_to - va_fr = $(rad2deg(min_va_diff))° in branch $min_va_diff_br"
    )
    
    return nothing
end

function check_area_power_balance(network_data::Dict{String,<:Any}, area=7)
    active_area_buses = Int64[
        b["index"] for b in values(network_data["bus"]) if 
        b["area"] == area && b["bus_type"] != 4
    ]
    active_area_loads = Int64[
        l["index"] for l in values(network_data["load"]) if
        l["load_bus"] ∈ active_area_buses && l["status"] == 1
    ]
    active_area_gens = Int64[
        g["index"] for g in values(network_data["gen"]) if
        g["gen_bus"] ∈ active_area_buses && g["gen_status"] == 1
    ]

    ### Active power balance
    area_pd = sum(
        Float64[network_data["load"]["$l"]["pd"] for l in active_area_loads]
    )
    area_pg = sum(
        Float64[network_data["gen"]["$g"]["pg"] for g in active_area_gens]
    )
    area_ploss = sum(
        Float64[br["pt"] + br["pf"] for br in values(network_data["branch"]) if 
        br["f_bus"] ∈ active_area_buses && br["t_bus"] in active_area_buses
        && br["br_status"] == 1]
    )

    area_Δp = area_pg - area_pd - area_ploss
    info(LOGGER, "Active power balance in area $area: $area_Δp")

    ### Reactive power balance
    area_qd = sum(
        Float64[network_data["load"]["$l"]["qd"] for l in active_area_loads]
    )
    area_qg = sum(
        Float64[network_data["gen"]["$g"]["qg"] for g in active_area_gens]
    )
    area_qloss = sum(
        Float64[br["qt"] + br["qf"] for br in values(network_data["branch"]) if 
        br["f_bus"] ∈ active_area_buses && br["t_bus"] in active_area_buses
        && br["br_status"] == 1]
    )
    area_qshunt = 0.
    for shunt in values(network_data["shunt"])
        if shunt["status"] == 1 && shunt["shunt_bus"] ∈ active_area_buses
            vm = network_data["bus"]["$(shunt["shunt_bus"])"]["vm"]
            area_qshunt += shunt["bs"] * vm^2
        end
    end

    area_Δq = area_qg - area_qd - area_qloss + area_qshunt
    info(LOGGER, "Reactive power balance in area $area: $area_Δq")

    return nothing
end