#* Functions for calculating the power flow in a power grid described by a NDD
#*------------------------------------------------------------------------------

"""
    calc_pf(file::String; kwargs...)

Reads a .RAW or .m `file` containing the data of a power grid and calculates the power flow according to the chosen model (see below). Returns a network data dictionary conform with the [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/), that also contains the calculated power flow solution.

# Keyword Arguments

`pf_model` can be set to `:ac` (default) or `:dc` and decides whether the AC power flow model or the DC power flow model is used. 

Additional keyword arguments are passed on to [`calc_ac_pf!`](@ref) or [`calc_dc_pf!`](@ref).
"""
function calc_pf(file::String; pf_model=:ac, import_all=false, kwargs...)
    network_data = parse_file(file, import_all=import_all) # read data file
    simplify_network!(network_data) # simplify topology, if possible
    
    ### Calculate power flow according to the chosen model
    if pf_model == :dc
        ### Initiate power flows on branches as zero without losses
        for br in values(network_data["branch"])
            br["pt"], br["pf"] = 0.0, 0.0
        end
        ### Rebalance active power dispatches without losses in branches
        restore_p_balance!(network_data)
        calc_dc_pf!(network_data; kwargs...) # calculate and add DC-PF
    elseif pf_model == :ac
        calc_ac_pf!(network_data; kwargs...) # calculate and add AC-PF
    else
        throw(ArgumentError("Unknown power flow model $(pf_model)!"))
    end

    return network_data
end

"""
    calc_ac_pf!(network_data::Dict{String,<:Any}; kwargs...)

Calculates an AC power flow solution for the given power grid described by the dictionary `network_data` and updates the state of the power grid (voltage variables, generator dispatches and branch flows are updated in `network_data` using the new solution). The form of `network_data` has to agree with the [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/). If a new AC solution was found and added to `network_data`, the latter will contain an entry `"pf_model" => :ac` indicating that an AC power flow was last calculated for the network. However, if the solver did not converge and `if_infeasible = :dc` was used (see **keyword arguments**), the new entry will be `"pf_model" => :dc`, if a DC solution was calculated and added to `network_data` instead.

# Keyword Arguments

`solver` can be set to `:Ipopt` (default) for a solver based on [Ipopt.jl](https://github.com/jump-dev/Ipopt.jl) or `:NLsolve` for a solver based on [NLsolve.jl](https://github.com/JuliaNLSolvers/NLsolve.jl) and specifies which solver should be used to solve the AC power flow model.

`print_level` can be set to a number between `0` and `12` (default `5`) and controls the verbosity level for console output of Ipopt, see [`Ipopt documentation`](https://coin-or.github.io/Ipopt/index.html) for details. `print_level` is not used, if NLsolve is used as the solver.

`use_warm_start`, if set to `true`, sets the previous state contained in `network_data` as initial conditions before attempting to solve the AC power flow equations (default `false`).

`if_infeasible` can be set to `:error` (default), `:dc` or `:continue`. `:error` throws an `ErrorException`, if the solver does not converge to an optimal AC solution. `:dc` tries to calculate a DC power flow solution, if the solver did not converge to an optimal AC solution (see [`calc_dc_pf!`](@ref)). `:continue` tries to use the AC solution returned by the solver as if it were an optimal solution (Ipopt usually returns a solution that minimizes constraint violations, if it detects a local infeasibility).

`output_file`, if set to a valid path, specifies the path of a .txt file to which Ipopt's output according to `print_level` is written (default `""`).
"""
function calc_ac_pf!(
        network_data::Dict{String,<:Any};
        solver = :Ipopt, # solver to use (:Ipopt or :NLsolve)
        print_level = 5, # verbosity level of Ipopt solver
        use_warm_start = false, # whether to use previous state as initial point
        if_infeasible = :save, # what to do, if solver did not converge 
        output_file = "", # optional file for Ipopt output
        error_file = joinpath(@__DIR__, "../applications/"),
        kwargs...
    )

    # Check whether to use the previous state as initial conditions
    if use_warm_start == true
        info(LOGGER, "Using previous state as initial conditions!")
        set_ac_pf_start_values!(network_data) 
    end

    if solver == :Ipopt 
        ### Calculate AC-PF solution using Ipopt.jl and a JuMP model
        result = run_ac_pf(
            network_data, 
            optimizer_with_attributes(
                Ipopt.Optimizer, 
                "print_level" => print_level,
                "output_file" => output_file
            )
        )

        ### Run garbage collector to close the optional output file
        if isempty(output_file) == false
            GC.gc() 
        end

        ### Check whether to catch possible infeasibilities
        if result["termination_status"] != TerminationStatusCode(4)
            warn(LOGGER, "Solver did not converge to an optimal AC solution!")
            if if_infeasible == :error
                throw(ErrorException(
                    "END: Ipopt did not converge to an optimal AC solution! Instead Ipopt returned: 
                    termination_status => $(result["termination_status"]), 
                    primal_status => $(result["primal_status"]), 
                    dual_status => $(result["dual_status"])"
                ))
            elseif if_infeasible == :save
                ### Save the current network for later analysis
                path = pwd()*"/ndd_acpf_error_"*randstring(5)*".jld2"
                save(path, "network_data", network_data)
                warn(LOGGER,
                    "Ipopt did not converge to an optimal AC solution! Saved the current network data at $path"
                )
                throw(ErrorException(
                    "END: Ipopt did not converge to an optimal AC solution! Instead Ipopt returned: 
                    termination_status => $(result["termination_status"]), 
                    primal_status => $(result["primal_status"]), 
                    dual_status => $(result["dual_status"])"
                ))
            elseif if_infeasible == :dc
                info(LOGGER, "Trying to calculate a DC power flow solution!")
                calc_dc_pf!(
                    network_data, 
                    print_level = print_level,
                    output_file = output_file
                )
            elseif if_infeasible == :continue
                result["pf_model"] = :ac
                update_pf_data!(network_data, result)
            end
        else
            result["pf_model"] = :ac
            update_pf_data!(network_data, result)
        end
    elseif solver == :NLsolve 
        ### Calculate AC-PF solution using NLsolve.jl
        result = compute_ac_pf(network_data, show_trace=true)
        ### Check whether to catch possible infeasibilities
        if result["termination_status"] == false
            if if_infeasible == :error
                throw(ErrorException("END: NLsolve did not converge!"))
            elseif if_infeasible == :dc
                info(LOGGER, "Trying to calculate a DC power flow solution!")
                calc_dc_pf!(
                    network_data,
                    print_level = print_level,
                    if_infeasible = :error,
                    output_file = output_file
                )
            end
        else
            result["pf_model"] = :dc
            update_pf_data!(network_data, result)
        end
    else
        throw(ArgumentError("Unknown solver $solver."))
    end

    return nothing
end

"""
    calc_dc_pf!(network_data::Dict{String,<:Any}; kwargs...)

Calculates a DC power flow solution for the given power grid described by the dictionary `network_data` and updates the state of the power grid (voltage angles and branch flows are updated in `network_data` using the new solution). The form of `network_data` has to agree with the [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/). The solution is always calculated using Ipopt. If a new DC solution was found and added to `network_data`, the latter will contain an entry `"pf_model" => :dc` indicating that a DC power flow was last calculated for the network. The DC model does not support warm starting.

# Keyword Arguments

`print_level` can be set to a number between `0` and `12` (default `5`) and controls the verbosity level for console output of Ipopt, see [`Ipopt documentation`](https://coin-or.github.io/Ipopt/index.html) for details.

`if_infeasible` can be set to `:error` (default) or `:continue`. `:error` throws an `ErrorException`, if the solver does not converge to an optimal solution. `:continue` tries to use the solution returned by the solver as if it were an optimal solution (Ipopt usually returns a solution that minimizes constraint violations, if it detects a local infeasibility).

`output_file`, if set to a valid path, specifies the path of a .txt file to which Ipopt's output according to `print_level` is written (default `""`).
"""
function calc_dc_pf!(
        network_data::Dict{String,<:Any}; 
        solver = :Ipopt, # solver to use (:Ipopt or :julia)
        print_level = 5, # verbosity level of Ipopt solver
        if_infeasible = :save, # what to do, if solver did not converge
        output_file = "" # optional file for Ipopt output
    )

    ### Calculate DC-PF solution using Ipopt.jl and a JuMP model
    if solver == :Ipopt
        result = run_dc_pf(
            network_data, 
            optimizer_with_attributes(
                Ipopt.Optimizer, 
                "print_level" => print_level,
                "output_file" => output_file # saves solver output, if not empty
            )
        )

        ### Run garbage collector to close the optional output file
        if isempty(output_file) == false
            GC.gc() 
        end

        ### Check whether to catch possible infeasibilities
        if result["termination_status"] != TerminationStatusCode(4)
            if if_infeasible == :error || if_infeasible == :dc
                throw(ErrorException(
                    "END: Ipopt did not converge to an optimal DC solution! Instead Ipopt returned: 
                    termination_status => $(result["termination_status"]), 
                    primal_status => $(result["primal_status"]), 
                    dual_status => $(result["dual_status"])"
                ))
            elseif if_infeasible == :save
                ### Save the current network for later analysis
                path = pwd() * "/data_dcpf_error_" * randstring(5) * ".jld2"
                save(path, "network_data", network_data, "result", result)
                warn(LOGGER,
                    "Ipopt did not converge to an optimal DC solution! Saved the current network data at $path"
                )
                throw(ErrorException(
                    "END: Ipopt did not converge to an optimal DC solution! Saved the current network data and result at $path. Instead Ipopt returned: 
                    termination_status => $(result["termination_status"]), 
                    primal_status => $(result["primal_status"]), 
                    dual_status => $(result["dual_status"])"
                ))
            end
        else
            result["pf_model"] = :dc
            update_pf_data!(network_data, result)
        end
    elseif solver == :Julia
        ### Calculate DC-PF solution using Julia's native linear system solvers
        result = compute_dc_pf(network_data)
        ### Check whether to catch possible infeasibilities
        if result["termination_status"] == false
            if if_infeasible == :error
                throw(ErrorException("END: NLsolve did not converge!"))
            end
        else
            result["pf_model"] = :dc
            update_pf_data!(network_data, result)
        end
    else
        throw(ArgumentError("Unknown solver $solver."))
    end

    return nothing
end

"""
    calc_secondary_dmg!(network_data::Dict{String,<:Any}, primary_dmg::Array{Tuple{String,Int64},1}; kwargs...)

Calculates cascading failures in a power grid described by the dictionary `network_data` that might be triggered during a sequence of wind-induced damages contained in `primary_dmg`. `primary_dmg` should be an `array` calculated via [`sim_primary_impact`](@ref). Returns the final network data dictionary containing the final state of the power grid.

# Keyword Arguments

`logpath`, if set to a valid path, specifies the path of a .log file to which logging messages will be written (default `""`).

There are several additional keyword arguments that control the way calculations are performed. These keyword arguments originate from inner functions like [`calc_new_state!`](@ref), [`run_outer_ac_pf!`](@ref), [`run_inner_ac_pf!`](@ref), [`calc_ac_pf!`](@ref) and [`calc_dc_pf!`](@ref).

`pf_model` can be set to `:ac` (default) or `:dc` and specifies the power flow model to be used for recalculating flows in the power grid.

`solver` can be set to `:Ipopt` (default) for a solver based on [Ipopt.jl](https://github.com/jump-dev/Ipopt.jl) or `:NLsolve` for a solver based on [NLsolve.jl](https://github.com/JuliaNLSolvers/NLsolve.jl) and specifies which solver should be used to solve the power flow model `pf_model`. **Please note** that the DC model (`pf_model = :dc`) for now only supports Ipopt as the solver.

`print_level` can be set to a number between `0` and `12` (default `5`) and controls the verbosity level for console output of Ipopt, see [`Ipopt's documentation`](https://coin-or.github.io/Ipopt/index.html). `print_level` is not used, if NLsolve is used as the solver.

`if_infeasible` can be set to `:error` (default), `:dc` or `:continue` and specifies what the algorithm should do, if solving the power flow problem becomes infeasible. `:error` throws an `ErrorException`, if the solver does not converge to an optimal solution (both for AC and DC power flow). `:dc` tries to calculate a DC power flow solution, if the solver did not converge to an optimal AC solution (only relevant if `pf_model = :ac` is used). `:continue` tries to use the solution returned by the solver as if it were an optimal solution (Ipopt usually returns a solution that minimizes constraint violations, if it detects a local infeasibility).


"""
function calc_secondary_dmg!(
        network_data::Dict{String,<:Any},
        primary_dmg::Array{Tuple{String,Int64},1}; # time series of wind damage
        save_supply_history = true, # whether to save time series of supply
        logpath = "", # optional path to .log file for saving logs of interest
        kwargs... # other keyword arguments (see documentation)
    )

    ### Check whether to initialize the logger
    if isempty(logpath) == false
        ### Add .log file to logger and set format
        push!(LOGGER, 
            DefaultHandler(
                logpath, DefaultFormatter("[{date} | {level} | {name}]: {msg}")
            )
        )
    end

    ### Get wind frames in which transmission lines are destroyed by the wind
    dmg_frames = sort(
        unique([primary_dmg[i][2] for i in 1:length(primary_dmg)])
    )

    ### Save the time series (history) of supplied load
    if save_supply_history == true
        network_data["history"] = Dict(
            f => Dict{Int64,Tuple{Float64,String,Array{Int64,1}}}() for f in dmg_frames
        )
    end

    pf_iter = 0 # increased by 1 every time the power flow is recalculated
    ### Go through wind frames and calculate cascading failures
    for f in dmg_frames
        ### Deactivate transmission lines destroyed by the wind in frame f 
        destroyed_tl = String[dmg[1] for dmg in primary_dmg if dmg[2] == f]
        info(LOGGER, 
            "Wind frame $f: Destroying overhead transmission lines $destroyed_tl"
        ) # log destroyed transmission lines
        destroy_tl!(network_data, destroyed_tl, f) # update network topology

        ### Calculate new state of the system
        pf_iter = calc_new_state!(network_data, pf_iter, f; kwargs...)

        ### Deactivate any overloaded branches and calculate cascade
        cascade_iter = 0 # count every time overloaded branches fail
        deactivated_br = deactivate_overloads!(network_data, f)
        while isempty(deactivated_br) == false
            cascade_iter += 1
            info(LOGGER, 
                "CF-iteration $cascade_iter: Cascading failure continues!"
            )
            ### Find new state of the system
            pf_iter = calc_new_state!(network_data, pf_iter, f; kwargs...)
            ### Deactivate new overloaded branches
            deactivated_br = deactivate_overloads!(network_data, f)
        end
    end

    if isempty(logpath) == false
        ### Close .log files after simulation
        for key in [h for h in keys(gethandlers(LOGGER)) if h != "console"]
            delete!(gethandlers(LOGGER), key)
        end
        GC.gc()
    end

    return network_data # return final network state
end

"""
    calc_new_state!(network_data::Dict{String,<:Any}, pf_iter = 1; kwargs...)

Attempts to calculate a new state of the power grid described by the dictionary `network_data` using a power flow model and optional voltage control loops specified in the **keyword arguments** (see below). `pf_iter` is an integer that is increased by one each time the power flow is recalculated. In order to find a new state, the power flow may have to be recalculated multiple times in inner voltage control loops (see [`run_outer_ac_pf!`](@ref) and [`run_inner_ac_pf!`](@ref)). Returns the final value of `pf_iter`. 

# Keyword Arguments

`pf_model` can be set to `:ac` (default) or `:dc` and specifies the power flow model to be used.

`save_pf_iter` can be used to save `network_data` as a .jld2 file before a specific power flow recalculation counted by `pf_iter`. In order to save the network data dictionary, the step of interest (value of `pf_iter`) and a valid path for .jld2 file have to be passed as a tuple to `save_pf_iter` (default `(0, "")`).

Additional keyword arguments are passed on to [`run_outer_ac_pf!`](@ref) or [`calc_dc_pf!`](@ref).
"""
function calc_new_state!(
        network_data::Dict{String,<:Any},
        pf_iter = 1, # count every time the power flow is recalculated
        f = 0; # current wind frame
        pf_model = :ac, # what power flow model to use
        save_pf_iter = (0, ""), # whether to save a specific pf iteration
        save_every_step = (false, ""), # whether to save all pf iterations
        kwargs... # optional keyword arguments
    )

    #TODO: Add the case pf_model = :acdc for DC-PF as backup, if AC-PF does not converge. If the system split into separate connected components, run_outer_ac_pf! etc. have to be called for each component separately! -> Can control functions like restore_p_balance! be improved for this case?
    #? Or is it possible to differentiate between connected components in calc_ac_pf!? How does the solution look like, if multiple components exist? Does it include information about which component did and which did not converge?

    ### Attempt to find a new state using the given power flow model and solver
    if pf_model == :ac
        pf_iter = run_outer_ac_pf!(
            network_data, pf_iter; save_pf_iter = save_pf_iter, kwargs...
        )
    elseif pf_model == :acdc
        ### Calculate connected components and treat them separately
        connected_components = calc_connected_components(network_data)
        if length(connected_components) == 1 # no split occurred
            pf_iter = run_outer_ac_pf!(
                network_data, pf_iter; save_pf_iter = save_pf_iter, kwargs...
            )
        else # more than one connected component exist in network_data
            for cc in connected_components
                cc_network_data = get_cc_data(network_data, cc)
                pf_iter = run_outer_ac_pf!(
                    cc_network_data, pf_iter; 
                    save_pf_iter = save_pf_iter, kwargs...
                )
            end
            
        end 
    elseif pf_model == :dc
        pf_iter += 1
        ### Check whether to save network_data before power flow recalculation
        if pf_iter == save_pf_iter[1]
            info(LOGGER, 
                "Saving network data dictionary of PF-iteration $pf_iter in $(save_pf_iter[2])"
            )
            save(save_pf_iter[2], "network_data", network_data)
        end

        ### Restore active power balance and calculate DC-PF
        correct_reference_buses!(network_data)
        restore_p_balance!(network_data, f)
        
        ### Check generator bounds
        check_gen_bounds(network_data)

        correct_reference_buses!(network_data)
        info(LOGGER,
            "PF-iteration $pf_iter: Calculating a new DC-PF solution."
        )
        output = @capture_out calc_dc_pf!(network_data; kwargs...)
        info(LOGGER, output) # log solver output
        
        ### Check generator bounds
        check_gen_bounds(network_data)

        if save_every_step[1] == true
            save(
                save_every_step[2]*"Texas_ndd_f$(f)_iter$(pf_iter).jld2", 
                "network_data", network_data, "frame", f, "pf_iter", pf_iter
            )
        end
    else
        throw(ArgumentError("Unknown power flow model $(pf_model)!"))
    end

    return pf_iter
end

"""
    run_outer_ac_pf!(network_data::Dict{String,<:Any}, pf_iter = 1; kwargs...)

Runs the outer AC voltage control loop that encompasses the inner AC control loop and performs _under-voltage load shedding_ (UVLS) using [`run_uvls!`](@ref), if activated (see **keyword arguments**). `pf_iter` is an integer that is increased by one each time the power flow is recalculated. Returns the final value of `pf_iter`.

# Keyword Arguments

`use_uvls` can be set to `true` (default) or `false` and specifies whether UVLS should be performed using [`check_voltage_bounds`](@ref) and [`run_uvls!`](@ref).

`uvls_step` should be a `Float64` (default `0.1`) that represents the load shedding step size in percentage of the original demand. Each time UVLS is performed, the demand of loads connected to a bus with under-voltage is reduced according to `uvls_step`.

`apb_tol` is a `Float64` (default `1e-10`) that specifies the tolerated mismatch in global active power balance (APB). APB is restored via [`restore_p_balance!`](@ref) prior to calculating any AC power flow.

`save_pf_iter` can be used to save `network_data` as a .jld2 file before a specific power flow recalculation counted by `pf_iter`. In order to save the network data dictionary, the step of interest (value of `pf_iter`) and a valid path for .jld2 file have to be passed as a tuple to `save_pf_iter` (default `(0, "")`).

Additional keyword arguments are passed on to [`run_inner_ac_pf!`](@ref).

See also: [`run_uvls!`](@ref), [`check_voltage_bounds`](@ref), [`restore_p_balance!`](@ref), [`run_inner_ac_pf!`](@ref)
"""
function run_outer_ac_pf!(
        network_data::Dict{String,<:Any},
        pf_iter = 1; # count every time the power flow is recalculated
        use_uvls = true, # whether to use under-voltage load shedding
        uvls_step = 0.1, # step size for under-voltage load shedding
        apb_tol = 1e-10, # mismatch tolerance for active power balance
        save_pf_iter = (0, ""), # save a specific power flow iteration
        kwargs... # optional keyword arguments
    )
    
    ### Restore active power balance and check generator bounds
    restore_p_balance!(network_data, apb_tol)
    check_gen_bounds(network_data; kwargs...)

    pf_iter = run_inner_ac_pf!(
        network_data, pf_iter; save_pf_iter=save_pf_iter, kwargs...
    )
    if use_uvls == true
        ### Get buses with under-voltages
        uv_buses = check_voltage_bounds(
            network_data, limit="lower", filter_active_loads=true
        )
        uvls_iter = 0 # counts under-voltage load shedding iterations
        while isempty(uv_buses) == false
            uvls_iter += 1
            ### Shed load and recalculate power flow
            run_uvls!(network_data, uv_buses, uvls_step)
            info(LOGGER, 
                "UVLS: Repeating inner AC control loop with reduced loads (UVLS iteration $uvls_iter)!"
            )
            pf_iter = run_inner_ac_pf!(
                network_data, pf_iter; save_pf_iter=save_pf_iter, kwargs...
            )
            ### Update buses with under-voltages
            uv_buses = check_voltage_bounds(
                network_data, limit="lower", filter_active_loads=true
            )
        end
        info(LOGGER,
            "UVLS finished!"
        )
    end

    return pf_iter
end

"""
    run_uvls!(network_data::Dict{String,<:Any}, uv_buses::Dict{String,Array{String,1}}, uvls_step=0.1)

Performs under-voltage load shedding (UVLS): Sheds active and reactive power load at under-voltage buses contained in `uv_buses` using the step size `uvls_step` in percentage of the original demand (default `0.1`). Each time UVLS is performed, the demand of loads connected to a bus with under-voltage is reduced according to `uvls_step`. `uv_buses` can be obtained via [`check_voltage_bounds`](@ref) using the keywords `limit = "lower"` and `filter_active_loads = true`. Loads in `network_data["load"]`, for which the demand was reduced, obtain an entry `uvls_iter` that counts each UVLS iteration at that load. Loads, whose demand is reduced to `0.0`, may become deactivated via `simplify_network!` provided by PowerModels.jl.

See also: [`run_outer_ac_pf!`](@ref), [`check_voltage_bounds`](@ref)
"""
function run_uvls!(
        network_data::Dict{String,<:Any},
        uv_buses::Dict{String,Array{String,1}}, # under-voltage buses with loads
        uvls_step = 0.1 # step size for UVLS in percentage
    )
    
    ### Go through buses with under-voltages and shed load, if possible
    for connected_loads in values(uv_buses)
        ### Go through the connected loads and shed load uniformly
        for l in connected_loads
            load = network_data["load"][l]
            ### Check whether load has been shed before
            if haskey(load, "uvls_count") == false
                load["uvls_count"] = 1 # first time load is shed
                load["uvls_N"] = ceil(Int64, 1/uvls_step) # maximum UVLS steps
                load["pd_original"] = load["pd"] # original active power load
                load["qd_original"] = load["qd"] # original reactive power load
            end
            ### Shed load according to the step size uvls_step, if possible
            if load["uvls_count"] < load["uvls_N"]
                pd, qd = load["pd"], load["qd"]
                new_pd = pd - uvls_step * load["pd_original"]
                new_qd = qd - uvls_step * load["qd_original"]
                info(LOGGER, 
                    "Reducing demand of load $(load["index"]) (bus $(load["load_bus"])) by $uvls_step of the original demand (UVLS-count $(load["uvls_count"])): pd = $pd --> pd = $new_pd and qd = $qd --> new_qd = $new_qd"
                )
                load["pd"] = new_pd
                load["qd"] = new_qd
                load["uvls_count"] += 1
            else
                ### Reduce load to 0.0
                #? Is deactivating the load better? Done by PowerModels anyway?
                warn(LOGGER,
                    "Demand of load $(load["index"]) (bus $(load["load_bus"])) is shed $(load["uvls_count"]) times => Set demand to 0.0"
                )
                load["pd"], load["qd"] = 0.0, 0.0
                load["uvls_count"] += 1
            end
        end
    end

    ### Check whether loads can be deactivated
    simplify_network!(network_data)

    return nothing
end

"""
    run_inner_ac_pf!(network_data::Dict{String,<:Any}, pf_iter = 1; kwargs...)

Runs the inner AC control loop that enforces reactive power limits of generators via the [PV-PQ Switching Logic](@ref), if activated with `enforce_q_limits = true` (see **keyword arguments**). `pf_iter` is an integer that is increased by one each time the power flow is recalculated. Returns the final value of `pf_iter`.

# Keyword Arguments

`enforce_q_limits` can be set to `true` (default) or `false` and specifies whether reactive power limits of generators should be enforced using [`run_pvpq_switching!`](@ref)

`save_pf_iter` can be used to save `network_data` as a .jld2 file before a specific power flow recalculation counted by `pf_iter`. In order to save the network data dictionary, the step of interest (value of `pf_iter`) and a valid path for .jld2 file have to be passed as a tuple to `save_pf_iter` (default `(0, "")`).

Additional keyword arguments are passed on to [`calc_ac_pf!`](@ref).
"""
function run_inner_ac_pf!(
        network_data::Dict{String,<:Any},
        pf_iter = 1; # count every time the power flow is recalculated
        enforce_q_limits = false, # whether to use PV-PQ switching
        save_pf_iter = (0, ""), # save a specific power flow iteration
        kwargs... # other keyword arguments
    )

    ### Make sure that all connected components have a reference bus defined
    correct_reference_buses!(network_data) # define reference buses
    
    ### Check whether to save the network data beforehand
    pf_iter += 1
    if pf_iter == save_pf_iter[1]
        info(LOGGER, 
            "Saving network data dictionary of PF-iteration $pf_iter in $(save_pf_iter[2])"
        )
        save(save_pf_iter[2], "network_data", network_data)
    end

    ### Recalculate AC-PF
    ref_ndd = deepcopy(network_data) # save current state for comparison later
    info(LOGGER,
        "PF-iteration $pf_iter: Calculating a new AC-PF solution."
    )
    output = @capture_out calc_ac_pf!(network_data; kwargs...)
    info(LOGGER, output) # log solver output
    check_ac_constraints(network_data, ref_ndd) # log constraints

    ### Check whether to enforce reactive power limits of generators
    if enforce_q_limits == true
        switched_buses = run_pvpq_switching!(network_data)
        pvpq_iter = 0 # counts PV-PQ switching iterations
        while isempty(switched_buses) == false
            pf_iter += 1
            pvpq_iter += 1
            info(LOGGER, 
                "PV-PQ Switching: Recalculating AC-PF with new bus types (PF-iteration $pf_iter, switching iteration $pvpq_iter)!"
            )
            output = @capture_out calc_ac_pf!(network_data; kwargs...)
            info(LOGGER, output) # log solver output
            check_ac_constraints(network_data, ref_ndd) # log constraints
            switched_buses = run_pvpq_switching!(network_data)
        end
        info(LOGGER,
            "PV-PQ switching converged after $pvpq_iter iterations."
        )
    end

    return pf_iter
end

"""
    run_pvpq_switching!(network_data::Dict{String,<:Any})

Switches bus types in `network_data` from PV to PQ and vice versa according to  the [PV-PQ Switching Logic](@ref). The purpose of this function is to enforce reactive power limits of generators. It is run automatically during [`calc_secondary_dmg!`](@ref), if `enforce_q_limits = true` (default) was chosen as a keyword argument.

See also: [`ITCPG._convert_gen_to_load!`](@ref) and [`ITCPG._convert_load_to_gen!`](@ref)
"""
function run_pvpq_switching!(network_data::Dict{String,<:Any})
    ### PQ-TO-PV SWITCHING
    ### Check voltage magnitudes of buses with active PQ-generators
    active_pq_gen_buses = [
        b for b in values(network_data["bus"]) if
        haskey(b, "has_active_pq_gen") == true && b["has_active_pq_gen"] == 1
    ]
    ### Check for over- and under-voltages at PQ-generators
    for bus in active_pq_gen_buses
        ### Get active PQ-generators connected to the bus
        active_pq_gens = [
            l for l in values(network_data["load"]) if
            l["load_bus"] == bus["index"] && haskey(l, "is_pq_gen") == true &&
            l["status"] == 1
        ]
        ### Check whether to convert PQ-generators saved as loads back to PV
        n_conv = 0 # count conversions of PQ-generators back to PV
        for (i, pq_gen) in enumerate(active_pq_gens)
            if pq_gen["used_qlim"] == "lower" && bus["vm"] < pq_gen["vset"]
                _convert_load_to_gen!(network_data, pq_gen)
                n_conv += 1
            elseif pq_gen["used_qlim"] == "upper" && bus["vm"] > pq_gen["vset"]
                _convert_load_to_gen!(network_data, pq_gen)
                n_conv += 1
            end
        end
        ### Check whether all connected PQ-generators were converted back to PV
        if n_conv == length(active_pq_gens)
            ### Bus no longer has an active PQ-generator
            bus["has_active_pq_gen"] = false 
        end
    end

    ### PV-TO-PQ SWITCHING
    ### Check whether reactive power limits of generators are exceeded
    for gen in [g for g in values(network_data["gen"]) if g["gen_status"] == 1]
        if gen["qg"] > gen["qmax"]
            ### Convert to PQ-generator fixed at upper Q-limit
            _convert_gen_to_load!(network_data, gen, "upper")
        elseif gen["qg"] < gen["qmin"]
            ### Convert to PQ-generator fixed at lower Q-limit
            _convert_gen_to_load!(network_data, gen, "lower")            
        end
    end

    ### Switch bus types according to new generators
    switched_buses = correct_bus_types!(network_data)
    return switched_buses
end

"""
    _convert_gen_to_load!(network_data::Dict{String,<:Any}, gen::Dict{String,<:Any}, qlim::String)

Turns a "PV-generator" described by the data dictionary `gen` (see [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/)) into a "PQ-generator" with the reactive power dispatch fixed at the limit `qlim` (`"upper"` or `"lower"`). In the context of the network data dictionary `network_data` this means that the generator will be deactivated in `network_data["gen"]` and a corresponding "load" is added to `network_data["load"]` (see below). As a consequence, the voltage constraint representing the generator set-point is dropped in the AC power flow model and the reactive power limit is enforced.

# Data Dictionary describing a PQ-Generator 

The following entry is added to `network_data["load"]`, if a PV-generator `gen` is converted to a PQ-generator for the first time. The structure of the dictionary is motivated by the [PowerModels Network Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/network-data/).

```julia
id::String => Dict{String,Any}(
    "source_id" => Any["load", gen["gen_bus"], "1 "],
    "load_bus" => gen["gen_bus"], # bus to which the PQ-generator is connected
    "status" => 1, # status of the PQ-generator
    "pd" => -gen["pg"], # active power dispatch
    "index" => length(network_data["load"]) + 1, # index of new "load"
    "is_gen" => gen["index"], # index of the corresponding PV-generator
    "vset" => gen["vg"], # voltage set-point of the PV-generator
    "used_qlim" => qlim # used reactive power limit
)
```
Note, that the key `id` is set to the string `"\$(length(network_data["load"] + 1))"` according to PowerModels conventions.

In addition, the PV-generator `gen` will be deactivated in `network_data["gen"]` and the following entries are added:

```julia
gen = Dict{String,Any}(
    "is_load" => length(network_data["load"]) + 1, # index of the PQ-generator
    "is_switched" => 1, # PV-generator is currently a PQ-generator ("load")
    "gen_status" => 0, # PV-generator is considered as inactive
    ... # other entries used by PowerModels
)
```
"""
function _convert_gen_to_load!(
        network_data::Dict{String,<:Any}, 
        gen::Dict{String,<:Any},
        qlim::String
    )
    
    ### Check whether the generator already was a PQ-generator (load) before
    if haskey(gen, "is_load") == true # corresponding load already exists
        ### Get corresponding load
        pq_gen = network_data["load"]["$(gen["is_load"])"]
        ### Make sure the load was deactivated before
        if pq_gen["status"] != 0
            throw(ErrorException(
                "Load $(pq_gen["index"]) corresponding to generator $(pq_gen["is_gen"]) was active, but should have been inactive!"
            ))
        end
        ### Reactivate and update the load representing the PQ-generator
        pq_gen["status"] = 1
        pq_gen["pd"] = -gen["pg"] # active power dispatch
        if qlim == "upper"
            pq_gen["qd"] = -gen["qmax"] # upper limit of reactive power dispatch
        elseif qlim == "lower"
            pg_gen["qd"] = -gen["qmin"] # lower limit of reactive power dispatch
        else
            throw(ArgumentError("Unknown reactive power limit $(qlim)!"))
        end
        pq_gen["used_qlim"] = qlim
    else
        ### Create a corresponding load
        index = length(network_data["load"]) + 1 # append to the loads
        network_data["load"]["$index"] = Dict{String,Any}(
            "source_id" => Any["load", gen["gen_bus"], "1 "],
            "load_bus" => gen["gen_bus"],
            "status" => 1,
            "pd" => -gen["pg"], # active power dispatch
            "index" => index,
            "is_gen" => gen["index"],
            "vset" => gen["vg"], # voltage setpoint
            "used_qlim" => qlim # used reactive power limit
        )
        ### Add reactive power dispatch according to qlim
        if qlim == "upper" 
            ### Fix generator at upper reactive power limit
            network_data["load"]["$index"]["qd"] = -gen["qmax"]
        elseif limit == "lower" 
            ### Fix generator at lower reactive power limit
            network_data["load"]["$index"]["qd"] = -gen["qmin"]
        else
            throw(ArgumentError("Unknown reactive power limit $(qlim)!"))
        end
        ### Assign this PQ-generator (load) to the original generator
        gen["is_load"] = index
        ### Save the information that the bus now has an active PQ-generator
        network_data["bus"]["$(gen["gen_bus"])"]["has_active_pq_gen"] = 1
    end

    gen["gen_status"] = 0 # deactivate the PV-generator
    gen["is_switched"] = 1 # is a PQ-generator now
    
    info(LOGGER, 
        "Converted generator $(gen["index"]) connected to bus $(gen["gen_bus"]) to a corresponding load $(gen["is_load"])."
    )

    return nothing
end

"""
    _convert_load_to_gen!(network_data::Dict{String,<:Any}, load::Dict{String,<:Any})

Turns a "PQ-generator" described by the data dictionary `load` (see [`_convert_gen_to_load!`](@ref)) into a "PV-generator" with the same active power dispatch. As a consequence, the reactive power dispatch becomes a variable and the voltage set-point of the generator is included as a constraint in the AC power flow model.
"""
function _convert_load_to_gen!(
        network_data::Dict{String,<:Any}, 
        load::Dict{String,<:Any}
    )
    
    ### Get corresponding generator and check whether it was deactivated
    gen = network_data["gen"]["$(load["is_gen"])"]
    if gen["gen_status"] != 0
        throw(ErrorException(
            "Generator $(gen["index"]) represented by load $(load["is_gen"]) was active, but should have been inactive!"
        ))
    end
    ### Deactivate load and reactivate generator
    load["status"] = 0
    gen["gen_status"] = 1
    gen["is_switched"] = 0
    ### Update dispatch
    gen["pg"] = -load["pd"]
    gen["qg"] = -load["qd"]

    info(LOGGER, 
        "Converted load $(load["index"]) connected to bus $(load["load_bus"]) back to the generator $(load["is_gen"])."
    )

    return nothing
end

#*---------------------------------------
#* Active Power Balance
#*---------------------------------------

"""
    restore_p_balance!(network_data::Dict{String,Any})

Restores active power balance (APB) in the network described by `network_data`.
"""
function restore_p_balance!(
        network_data::Dict{String,<:Any}, f = 0; 
        apb_tol = 1e-10
    )

    ### Calculate connected components
    connected_components = calc_connected_components(network_data)

    ### Restore active power balance for every connected component
    for (cc_i, cc) in enumerate(connected_components)
        _restore_p_balance!(network_data, cc, cc_i, f, apb_tol)
    end

    ### Update network topology (remove dangling buses etc.)
    simplify_network!(network_data) #! Has to be reworked due to PQ-generators!

    return nothing
end

"""
    _restore_p_balance!(network_data::Dict{String,<:Any}, cc::Set{Int64}, cc_i::Int64)

Restores active power balance (APB) in a connected component contained in the network data dictionary `network_data` and identified by its set of buses `cc` 
Called by [`restore_p_balance!`](@ref).
"""
function _restore_p_balance!(
        network_data::Dict{String,<:Any}, 
        cc::Set{Int64}, # set of bus indices in connected component
        cc_i::Int64, # index/number of connected component
        f = 0, # wind frame for saving currently supplied load
        apb_tol = 1e-10 # mismatch tolerated for active power balance
    )
    
    active_cc_gens = Int64[
        g["index"] for g in values(network_data["gen"]) if 
        g["gen_bus"] ∈ cc && g["gen_status"] != 0
    ] # indices of active PV-generators that are connected to buses in cc
    # active_cc_pq_gens = Int64[
    #     l["index"] for l in values(network_data["load"]) if 
    #     l["load_bus"] ∈ cc && l["status"] != 0 && haskey(l, "is_gen") == true
    # ] # indices of active PQ-generators that are connected to buses in cc

    active_cc_loads = Int64[
        l["index"] for l in values(network_data["load"]) if 
        l["load_bus"] ∈ cc && l["status"] != 0 && haskey(l, "is_gen") == false
    ] # indices of active loads that are connected to buses in cc

    ### Calculate active power mismatch
    cc_Δp, cc_Δp_lim = calc_Δp(
        network_data, active_cc_gens, active_cc_loads, cc
    )

    if cc_Δp > apb_tol # overproduction
        info(LOGGER, "Grid #$cc_i: Detected overproduction: $cc_Δp")
        ### Check how to restore APB (if possible)
        if isapprox(cc_Δp_lim, 0, atol=apb_tol) # power balance at minimum
            info(LOGGER, 
                "Grid #$cc_i: Setting active power dispatches to their lower bounds!"
            )
            ### Set all active generators to their minimum
            for g in active_cc_gens
                id = string(g)
                network_data["gen"][id]["pg"] = network_data["gen"][id]["pmin"]
            end
        elseif cc_Δp_lim < -apb_tol # sufficient dispatch reduction possible
            info(LOGGER, 
                "Grid #$cc_i: Global active power mismatch requires a reduction of generation!"
            )
            ### Redistribute dispatches of slack generators according to limits
            redistribute_slack_dispatches!(network_data, active_cc_gens)
            ### Reduce all dispatches uniformly
            cc_Δp = _reduce_p_dispatch!(
                network_data, active_cc_gens, active_cc_loads, cc, cc_Δp
            )
            info(LOGGER, 
                "Grid #$cc_i: Global active power mismatch with reduced generation: $cc_Δp"
            )
        else # overproduction is unavoidable
            ### Fail complete component
            info(LOGGER, 
                "Grid #$cc_i: Overproduction is unavoidable => #$cc_i fails!"
            )
            _deactivate_cc!(network_data, cc, active_cc_gens, active_cc_loads)
            if haskey(network_data, "history")
                supply_f = network_data["history"][f]
                supply_f[length(supply_f)+1] = (
                    get_total_load(network_data)[1], "overproduction", Int64[]
                )
            end
        end
    elseif cc_Δp < -apb_tol # underproduction
        info(LOGGER, "Grid #$cc_i: Detected underproduction: $cc_Δp")
        ### Check how to restore APB
        if isapprox(cc_Δp_lim, 0, atol=apb_tol) # power balance at maximum
            info(LOGGER, 
                "Grid #$cc_i: Setting active power dispatches to their upper bounds!"
            )
            ### Set all active generators to their maximum
            for g in active_cc_gens
                id = string(g)
                network_data["gen"][id]["pg"] = network_data["gen"][id]["pmax"]
            end
        elseif cc_Δp_lim < -apb_tol # total demand cannot be matched
            info(LOGGER, 
                "Grid #$cc_i: Global active power mismatch requires load shedding!"
            )
            ### Set all active generators to their maximum
            for g in active_cc_gens
                id = string(g)
                network_data["gen"][id]["pg"] = network_data["gen"][id]["pmax"]
            end
            info(LOGGER, 
                "Grid #$cc_i: Amount of active power load to be shed: $cc_Δp_lim"
            )
            ### Shed the minimum amount of load
            cc_Δp = _shed_minimum_load!(
                network_data, active_cc_gens, active_cc_loads, cc, cc_Δp_lim
            )
            info(LOGGER, 
               "Grid #$cc_i: Global active power mismatch after load shedding: $cc_Δp"
            )
            if haskey(network_data, "history")
                supply_f = network_data["history"][f]
                supply_f[length(supply_f)+1] = (
                    get_total_load(network_data)[1], "load_shedding", Int64[]
                )
            end
        else # sufficient dispatch increase possible
            info(LOGGER, 
                "Grid #$cc_i: Global active power mismatch requires an increase of generation!"
            )
            ### Redistribute dispatches of slack generators according to limits
            redistribute_slack_dispatches!(network_data, active_cc_gens)
            ### Increase all dispatches uniformly
            cc_Δp = _increase_p_dispatch!(
                network_data, active_cc_gens, active_cc_loads, cc, cc_Δp
            )
            info(LOGGER, 
                "Grid #$cc_i: Global active power mismatch with increased generation: $cc_Δp"
            )
        end
    else # mismatch within tolerance
        info(LOGGER, 
            "Grid #$cc_i: Global active power mismatch within tolerance: $cc_Δp"
        )
    end

    return nothing
end

#=
Calculates and returns the active power mismatch in a connected component identified by the set of buses cc. Depending on whether an overproduction or underproduction exists, the limiting mismatch with minimum or maximum possible active power generation is also calculated and returned.
=#
function calc_Δp(
        network_data::Dict{String,<:Any},
        active_cc_gens::Array{Int64,1}, 
        active_cc_loads::Array{Int64,1},
        cc::Set{Int64} # set of bus indices in connected component
    )

    ### Calculate active power demand in connected component
    cc_pd = sum(
        Float64[network_data["load"]["$l"]["pd"] for l in active_cc_loads]
    )
    
    ### Calculate total active power loss in branches in connected component
    cc_ploss = sum(
        Float64[br["pt"] + br["pf"] for br in values(network_data["branch"]) 
        if br["f_bus"] ∈ cc && br["t_bus"] in cc && br["br_status"] == 1]
    )

    ### Calculate active power generation in connected component
    cc_pg = sum(
        Float64[network_data["gen"]["$g"]["pg"] for g in active_cc_gens]
    )

    ### Calculate relevant mismatches
    cc_Δp = cc_pg - cc_pd - cc_ploss 
    if isapprox(cc_Δp, 0, atol=1e-10) # "sufficient" power balance
        cc_Δp = 0.0
        cc_Δp_lim = NaN # limiting mismatch not needed
    elseif cc_Δp > 0 # overproduction
        cc_pmin = sum(
            Float64[network_data["gen"]["$g"]["pmin"] for g in active_cc_gens]
        ) # minimum possible active power dispatch
        cc_Δp_lim = cc_pmin - cc_pd - cc_ploss # limiting mismatch
    elseif cc_Δp < 0 # underproduction
        cc_pmax = sum(
            Float64[network_data["gen"]["$g"]["pmax"] for g in active_cc_gens]
        ) # maximum possible active power dispatch
        cc_Δp_lim = cc_pmax - cc_pd - cc_ploss # limiting mismatch
    end

    return cc_Δp, cc_Δp_lim
end

function redistribute_slack_dispatches!(
        network_data::Dict{String,<:Any},
        active_cc_gens::Array{Int64,1}
    )

    slack_gens = [
        network_data["gen"]["$g"] for g in active_cc_gens if
        network_data["bus"]["$(network_data["gen"]["$g"]["gen_bus"])"]["bus_type"] == 3
    ]
    @assert length(slack_gens) > 0 "Slack bus has no active generators!"    

    ### Calculate aggregated dispatch and aggregated bounds
    sum_pmin, sum_pmax, sum_pg = 0.0, 0.0, 0.0 # active power
    for g in slack_gens
        sum_pmin += g["pmin"]
        sum_pmax += g["pmax"]
        sum_pg += g["pg"]
    end

    total_pg = sum_pg # total generation to redistribute
    if isapprox(sum_pg, sum_pmin, atol=1.0e-7) # sum_pg == sum_pmin
        for g in slack_gens
            g["pg"] = g["pmin"]
        end
    elseif isapprox(sum_pg, sum_pmax, atol=1.0e-7) # sum_pg == sum_pmax
        for g in slack_gens
            g["pg"] = g["pmax"]
        end
    elseif sum_pg > sum_pmin && sum_pg < sum_pmax
        ### Initialize all generators at minimum dispatch
        for g in slack_gens
            g["pg"] = g["pmin"]
        end
        ### Calculate remaining discrepancy in active power dispatch
        Δp = sum_pg - sum_pmin
        while !isapprox(Δp, 0.0, atol=1.0e-7)
            ### Find generators capable of increasing their dispatch
            capable_slack_gens  = [g for g in slack_gens if g["pg"] < g["pmax"]]
            Δ_pg = Δp / length(capable_slack_gens)
            min_capability = minimum(
                g -> (g["pmax"] - g["pg"]), capable_slack_gens
            )
            if Δ_pg <= min_capability # no generator hits its maximum dispatch
                for g in capable_slack_gens
                    g["pg"] += Δ_pg
                    Δp -= Δ_pg
                end
            else # at least one generator will hit its maximum dispatch
                for g in capable_slack_gens
                    if (g["pmax"] - g["pmin"]) == min_capability
                        g["pg"] = g["pmax"]
                    else
                        g["pg"] += min_capability
                    end
                    Δp -= min_capability
                end
            end
        end
    else
        warn(LOGGER,
            "Aggregated dispatch $sum_pg violates aggregated capacity interval [$sum_pmin, $sum_pmax]!"
        )
    end

    # println([(g["pg"], g["pmin"], g["pmax"]) for g in slack_gens])

    return nothing
end

#=
Uniformly reduces active power dispatches of active generators in a connected component according to the active power mismatch. This is done recursively, since generators may hit their minimum dispatch, which has to be compensated by other generators.
=#
function _reduce_p_dispatch!(
        network_data::Dict{String,<:Any},
        active_cc_gens::Array{Int64,1}, # active generators in cc
        active_cc_loads::Array{Int64,1}, # active loads in cc
        cc::Set{Int64}, # set of bus indices in connected component
        cc_Δp::Float64 # mismatch larger than tolerance
    )

    ### Find active generators that are able to reduce their dispatch
    capable_cc_gens = Int64[
        g for g in active_cc_gens if 
        network_data["gen"]["$g"]["pg"] > network_data["gen"]["$g"]["pmin"]
    ]
    g_Δp = cc_Δp / length(capable_cc_gens) # power reduction for each generator
    # println(g_Δp)
    # println(capable_cc_gens)

    ### Reduce dispatches as much as possible 
    for g in capable_cc_gens
        gen = network_data["gen"]["$g"]
        if g_Δp >= gen["pg"] - gen["pmin"] # generator reaches minimum dispatch
            gen["pg"] = gen["pmin"]
        else # generator can reduce dispatch sufficiently
            gen["pg"] -= g_Δp
        end
    end

    ### Recalculate active power mismatch and repeat reduction, if necessary
    cc_Δp, cc_Δp_lim = calc_Δp(
        network_data, active_cc_gens, active_cc_loads, cc
    )
    if !isapprox(cc_Δp, 0, atol=1e-10) # power balance still not restored
        debug(LOGGER, "Repeating dispatch reduction with mismatch: $cc_Δp")
        cc_Δp = _reduce_p_dispatch!(
            network_data, active_cc_gens, active_cc_loads, cc, cc_Δp
        ) # repeat
    end

    # return nothing
    return cc_Δp
end

#=
Uniformly increases active power dispatches of active generators in a connected component according the active power mismatch. This is done recursively, since generators may hit their maximum dispatch, which has to be compensated by other generators.
=#
function _increase_p_dispatch!(
        network_data::Dict{String,<:Any},
        active_cc_gens::Array{Int64,1},
        active_cc_loads::Array{Int64,1},
        cc::Set{Int64}, # set of bus indices in connected component
        cc_Δp::Float64 # mismatch smaller than tolerance
    )
    
    ### Find active generators that are able to increase their dispatch
    capable_cc_gens = Int64[
        g for g in active_cc_gens if 
        network_data["gen"]["$g"]["pg"] < network_data["gen"]["$g"]["pmax"]
    ]
    g_Δp = abs(cc_Δp) / length(capable_cc_gens) # power increase per generator

    ### Increase dispatches as much as possible 
    for g in capable_cc_gens
        gen = network_data["gen"]["$g"]
        if g_Δp >= gen["pmax"] - gen["pg"] # generator reaches maximum dispatch
            gen["pg"] = gen["pmax"]
        else # generator can increase dispatch sufficiently
            gen["pg"] += g_Δp
        end
    end

    ### Recalculate active power mismatch and repeat increase, if necessary
    cc_Δp, cc_Δp_lim = calc_Δp(
        network_data, active_cc_gens, active_cc_loads, cc
    )
    if !isapprox(cc_Δp, 0, atol=1e-10) # power balance still not restored
        cc_Δp = _increase_p_dispatch!(
            network_data, active_cc_gens, active_cc_loads, cc, cc_Δp
        ) # repeat
    end

    return cc_Δp
end

#=
Sheds the amount of load necessary to restore active power balance in a connected component. If active generators are already operated at their maximum possible dispatch, the amount of load shed should be optimal (minimal). 
=#
function _shed_minimum_load!(
        network_data::Dict{String,<:Any},
        active_cc_gens::Array{Int64,1},
        active_cc_loads::Array{Int64,1},
        cc::Set{Int64}, # set of bus indices in connected component
        cc_Δp_lim::Float64 # negative limiting mismatch
    )

    ### Find active loads that are able to reduce their demand
    capable_cc_loads = Int64[
        l for l in active_cc_loads if network_data["load"]["$l"]["pd"] > 0
    ]
    l_Δp = abs(cc_Δp_lim) / length(capable_cc_loads) # demand reduction per load

    ### Reduce demands as much as possible 
    for l in capable_cc_loads
        load = network_data["load"]["$l"]
        if l_Δp >= load["pd"] # load reaches zero demand
            load["pd"] = 0.0
        else # load can reduce its demand sufficiently
            load["pd"] -= l_Δp
        end
    end

    ### Recalculate active power mismatch and repeat load shedding, if necessary
    cc_Δp, cc_Δp_lim = calc_Δp(
        network_data, active_cc_gens, active_cc_loads, cc
    )
    if !isapprox(cc_Δp, 0, atol=1e-10) # power balance still not restored
        cc_Δp = _shed_minimum_load!(
            network_data, active_cc_gens, active_cc_loads, cc, cc_Δp_lim
        ) # repeat
    end

    return cc_Δp
end

#*------------------------------------------------------------------------------
#* Utility Functions
#*------------------------------------------------------------------------------

"""
    update_pf_data!(network_data::Dict{String,<:Any}, pf_result::Dict{String,<:Any})

Writes the power flow solution contained in `pf_result` to the network data dictionary `network_data`. `pf_result` should be a dictionary according to the [PowerModels Result Data Format](https://lanl-ansi.github.io/PowerModels.jl/stable/result-data/#The-Result-Data-Dictionary) and contain an **additional entry** `"pf_model"` that identifies the power flow model that was used to obtain `pf_result` (e.g. `:ac` or `:dc`). `"pf_model"` is automatically set in functions like [`calc_ac_pf!`](@ref) and[`calc_dc_pf!`](@ref). Returns the network data dictionary containing the new bus voltages, generator dispatches and branch power flows (calculated according to `"pf_model"`).
"""
function update_pf_data!(
        network_data::Dict{String,<:Any}, 
        pf_result::Dict{String,<:Any}
    )
    
    ### Add solution (voltage angles and magnitudes) to NDD
    update_data!(network_data, pf_result["solution"])
    
    ### Calculate power flows with PowerModels.jl according to the used model
    pf_model = pf_result["pf_model"]
    if pf_model == :dc
        flows = calc_branch_flow_dc(network_data) # calc. DC branch flows
    elseif pf_model == :ac
        flows = calc_branch_flow_ac(network_data) # calc. AC branch flows
    else
        throw(ArgumentError("Unknown power flow model $(pf_model)!"))
    end

    update_data!(network_data, flows) # add power flows to NDD
    network_data["pf_model"] = pf_model # add used model to NDD
    calc_branchloads!(network_data) # calculate branch loadings

    return network_data
end

"""
    calc_branchloads!(network_data::Dict{String,<:Any})

Calculates the loading of branches (flow/capacity) using the power flow solution contained in the network data dictionary `network_data` and writes it to the specific branch dictionaries in `network_data["branch"]`. If `network_data` contains an AC power flow solution, three types of branch loadings are computed: i) the loading due to active power (`"MW-loading"`), ii) the loading due to reactive power (`"Mvar-loading"`), and iii) the loading due to apparent power (`"MVA-loading"`). If `network_data` contains a DC power flow solution, only the loading due to active power is calculated (`"MW-loading"`).
"""
function calc_branchloads!(network_data::Dict{String,<:Any})
    ### Go through branches and calculate loadings
    pf_model = network_data["pf_model"]
    for (i, branch) in network_data["branch"]
        if pf_model == :dc # DC-PF
            ### Active power flow
            mw_fr = sqrt(branch["pf"]^2)
            mw_to = sqrt(branch["pt"]^2)
            branch["MW-loading"] = max(mw_fr, mw_to) / branch["rate_a"]
        elseif pf_model == :ac # AC-PF
            ### Active power flow
            mw_fr = sqrt(branch["pf"]^2)
            mw_to = sqrt(branch["pt"]^2)
            branch["MW-loading"] = max(mw_fr, mw_to) / branch["rate_a"]
            ### Reactive power flow
            mvar_fr = sqrt(branch["qf"]^2)
            mvar_to = sqrt(branch["qt"]^2)
            branch["Mvar-loading"] = max(mvar_fr, mvar_to) / branch["rate_a"]
            ### Apparent power flow
            mva_fr = sqrt(branch["pf"]^2 + branch["qf"]^2)
            mva_to = sqrt(branch["pt"]^2 + branch["qt"]^2)
            branch["MVA-loading"] = max(mva_fr, mva_to) / branch["rate_a"]
        else
            throw(ArgumentError("Unknown power flow model $(pf_model)!"))
        end
    end

    return nothing
end