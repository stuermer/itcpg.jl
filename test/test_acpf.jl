#*------------------------------------------------------------------------------
#* Test whether the AC power flow solution obtained with
#* PowerModels.jl (PM) agrees with the PowerWold Simulator (PWS) solution for
#* the synthetic grid of Texas
#*------------------------------------------------------------------------------

using Test
using ITCPG
using JLD2
using FileIO
using CSV
using DataFrames

#*------------------------------------------------------------------------------
#* Load AC-PF solution calculated with PowerModels.jl

ndd_ac = load(joinpath(@__DIR__, "Texas_init_acpf.jld2"), "network_data")

#*------------------------------------------------------------------------------
#* Read CSV-file containing AC-PF solution calculated with PowerWorld Simulator 

BranchData = CSV.File(joinpath(@__DIR__, "Texas_BranchData.csv")) |> DataFrame

#*------------------------------------------------------------------------------
#* Compare loading of all branches

@test nrow(BranchData) == length(ndd_ac["branch"])
L = nrow(BranchData) # number of branches

PWS_bl = zeros(L)
PM_bl = zeros(L)
baseMVA = ndd_ac["baseMVA"]

### Go through branches in PWS data and find corresponding PM branch loadings
for (i, br) in enumerate(eachrow(BranchData))
    circ = br[:Circuit] # circuit number of branch
    branch = [br[:BusNumFrom], br[:BusNumTo], "$circ "]
    branch_rev = [br[:BusNumTo], br[:BusNumFrom], "$circ "]
    PWS_bl[i] = br[:PercentMVA]/baseMVA # branch load from PWS data

    ### Find corresponding branch in PM data
    if br[:BranchDeviceType] == "Line" # branch is a transmission line
        br_id = findall(
            x -> (
                [x["f_bus"], x["t_bus"], x["source_id"][end]] == branch || 
                [x["f_bus"], x["t_bus"], x["source_id"][end]] == branch_rev
            ),
            ndd_ac["branch"]
        )[1]
    else # branch is a transformer
        br_id = findall(
            x -> (
                [x["f_bus"], x["t_bus"], x["source_id"][end-1]] == branch ||
                [x["f_bus"], x["t_bus"], x["source_id"][end-1]] == branch_rev
            ),
            ndd_ac["branch"]
        )[1]
    end

    ### Assign flows according to branch activities
    if ndd_ac["branch"][br_id]["br_status"] == 0 # branch is inactive
        PM_bl[i] = 0.0
    else # branch is active
        PM_bl[i] = ndd_ac["branch"][br_id]["MVA-loading"]
    end
end

@test isapprox(PM_bl, PWS_bl, atol=.0001)